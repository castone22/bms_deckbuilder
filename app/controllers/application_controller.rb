class ApplicationController < ActionController::Base

  # respond_to :json, :html, :turbo_stream

  def after_sign_in_path_for(resource)
    decks_path
  end

  # def after_sign_up_path_for(resource)
  #   new_user_session
  # end
end
