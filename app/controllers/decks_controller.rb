class DecksController < ApplicationController
  before_action :authenticate_user!, except: %i[draw index draw_3]
  before_action :set_deck, only: %i[ show edit update destroy draw draw_3 ]

  def draw
    draw = @deck.cards.select{|it| it.enabled }.sample
    respond_to do |format|
      format.html { render html: "<br><span style=\"color:#{draw.color || "black"};font-size:30px;text-align: center\">#{draw.body}</span>".html_safe }
    end
  end

  def draw_3
    draws = @deck.cards.select{|it| it.enabled }.sample(3)
    respond_to do |format|
      html = ("<br>" + draws.each_with_index.map { |draw, i| "<span style=\"color:#{draw.color || "black"};font-size:30px;text-align: center\">#{draw.body}#{((i < 2) && ", ") || ""} </span>"}.join("")).html_safe
      format.html { render html: html }
    end
  end

  # GET /decks or /decks.json
  def index
    @decks = Deck.all
  end

  # GET /decks/1 or /decks/1.json
  def show
  end

  # GET /decks/new
  def new
    @deck = Deck.new
  end

  # GET /decks/1/edit
  def edit
  end

  # POST /decks or /decks.json
  def create
    @deck = Deck.new(deck_params)
    @deck.slug = @deck.name.downcase.gsub(' ', '-')
    respond_to do |format|
      if @deck.save
        format.html { redirect_to deck_url(@deck), notice: "Deck was successfully created." }
        format.json { render :show, status: :created, location: @deck }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @deck.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /decks/1 or /decks/1.json
  def update
    deck_params[:slug] ||= deck_params[:name].downcase.gsub(' ', '-')
    respond_to do |format|
      if @deck.update(deck_params)
        format.html { redirect_to deck_url(@deck), notice: "Deck was successfully updated." }
        format.json { render :show, status: :ok, location: @deck }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @deck.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /decks/1 or /decks/1.json
  def destroy
    @deck.destroy

    respond_to do |format|
      format.html { redirect_to decks_url, notice: "Deck was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deck
      @deck = Deck.find_by_slug(params[:id]) if params[:id] =~ /\p{L}/
      @deck ||= Deck.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def deck_params
      params.require(:deck).permit(:name, :slug, cards_attributes: [:id, :body, :enabled, :color, :_destroy])
    end
end
