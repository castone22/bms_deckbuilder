{
  packageOverrides = pkgs: with pkgs; {
    ruby = pkgs.ruby_3_0;
    bundler = pkgs.bundler.override { inherit ruby; };
    bundix = pkgs.bundix.override { inherit bundler; };
  };
}