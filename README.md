# README

Mostly just a weird spike for rails 7 that morphed into a proof of
concept for using vue 3 natively with the new import maps.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
