class AddSlugsToDecks < ActiveRecord::Migration[7.0]
  def change
    add_column :decks, :slug, :string
    add_index :decks, :slug, unique: true
  end
end
