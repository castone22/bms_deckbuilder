class AddEnabledToCards < ActiveRecord::Migration[7.0]
  def change
    add_column :cards, :enabled, :boolean
  end
end
