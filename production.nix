# with (import <nixpkgs> { config = import ./config.nix;});
{ pkgs, stdenv, git, bundler, bundix, clang, libxml2, libxslt, readline, postgresql, openssl, bundlerEnv, libiconv, zlib, ...}:

let
  ruby = pkgs.ruby_3_0;
  bundler = pkgs.bundler.override { inherit ruby; };
  bundix = pkgs.bundix.override { inherit bundler; };
  rubyenv = bundlerEnv {
    name = "rb";
    # Setup for ruby gems using bundix generated gemset.nix
    gemfile = ./Gemfile;
    lockfile = ./Gemfile.lock;
    gemset = ./gemset.nix;
    # Bundler groups available in this environment
    groups = ["default" "production"];
  };
  
in stdenv.mkDerivation {
  name = "bms-deck-builder";
  version = "0.0.1";

  src = /home/lridge/devl/bms_deckbuilder;

  buildInputs = [
    stdenv
    git

    # Ruby deps
    ruby
    bundler
    bundix

    # Rails deps
    clang
    libxml2
    libxslt
    readline
    postgresql
    openssl

    rubyenv
  ];

  installPhase = ''
    bundix -l
    cp -r $src $out
  '';

  shellHook = ''
    export LIBXML2_DIR=${pkgs.libxml2}
    export LIBXSLT_DIR=${pkgs.libxslt}
  '';
}