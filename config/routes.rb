Rails.application.routes.draw do
  devise_for :users

  resources :decks
  get 'decks/:id/draw', controller: :decks, action: :draw
  get 'decks/:id/draw_3', controller: :decks, action: :draw_3
  root "decks#index"

  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
